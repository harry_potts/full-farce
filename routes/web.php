<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => 'api'], function ($router) {
    $router->group(['prefix' => 'words'], function ($router) {
        $router->get('/categories', 'WordsController@categories');
        $router->get('/category/{category_id}', 'WordsController@categoryWords');
    });
});

$router->get('/{route:.*}/', function ()  {
    return view('app');
});