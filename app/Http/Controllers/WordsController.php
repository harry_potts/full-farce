<?php

namespace App\Http\Controllers;

use App\Game\Category;
use App\Game\Word;

class WordsController extends Controller
{

    protected $category_model;
    protected $word_model;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Category $category, Word $word)
    {
        $this->category_model = $category;
        $this->word_model = $word;
    }

    public function categories()
    {
        $word_categories = $this->category_model->whereHas('words')->with('words')->get();
        return $word_categories;
    }

    public function categoryWords($category_id)
    {
        $words = $this->word_model->whereHas('category', function($category) use($category_id) {
            $category->where('id', $category_id);
        })->get();
        return $words;
    }
}
