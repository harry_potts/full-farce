<?php

namespace App\Game;

use Illuminate\Database\Eloquent\Model;

class Word extends Model
{

    public function category()
    {
        return $this->belongsTo('App\Game\Category');
    }
    
}
