<?php

namespace App\Game;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    public function words()
    {
        return $this->hasMany('App\Game\Word');
    }

}
