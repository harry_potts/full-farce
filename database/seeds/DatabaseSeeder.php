<?php

use Illuminate\Database\Seeder;
use App\Game\Word;
use App\Game\Category;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $animals = Category::create([
            'name' => 'Animals'
        ]);

        factory(Word::class, 10)->create()->each(function ($word) use ($animals) {
            $word->category()->associate($animals);
            $word->save();
        });
        
    }
}
