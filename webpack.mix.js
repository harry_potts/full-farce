let mix = require('laravel-mix');

mix.js('resources/js/app.js', 'dist/').version();
mix.sass('resources/scss/app.scss', 'dist/').version();