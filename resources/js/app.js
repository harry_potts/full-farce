import Vue from 'vue'
import VueRouter from 'vue-router'
import VueCompositionApi from '@vue/composition-api'
import Vuex from 'vuex'

import axios from 'axios'

import GameSelect from './components/GameSelect.vue'
import WordsCategories from './components/WordsCategories.vue'
import WordsCategory from './components/WordsCategory.vue'

Vue.use(VueRouter)
Vue.use(VueCompositionApi)
Vue.use(Vuex)

Vue.mixin({
    data: () => ({
        skip: new Audio('/audio/skip.flac'),
        correct: new Audio('/audio/correct.wav'),
        end: new Audio('/audio/end.wav'),
        start: new Audio('/audio/start.wav'),
        click: new Audio('/audio/click.wav')
    }),
    methods: {
        playAudio(type)
        {
            let audio = null

            switch(type) {
                case 'skip':
                    audio = this.skip
                    audio.volume = 1
                    break;
                case 'correct':
                    audio = this.correct
                    audio.volume = 1
                    break;
                case 'end':
                    audio = this.end
                    audio.volume = 1
                    break;
                case 'start':
                    audio = this.start
                    audio.volume = 0.3
                    break;
                case 'click':
                    audio = this.click
                    audio.volume = 0.7
                    break;
            }

            if (audio) {
                audio.currentTime = 0
                audio.play()
            }
        }
    }
})

Vue.mixin({
    methods: {
        storeCategories() {
            if (!this.$store.state.wordCategories.length) {
                this.$axios.get('/api/words/categories').then((response) => {
                    for (let i in response.data) {
                        let wordCategory = response.data[i]
                        this.$store.commit('addWordCategory', wordCategory)
                    }
                })
            }
        }
    }
})

Vue.prototype.$axios = axios

const router = new VueRouter({
    mode: 'history',
    base: '/',
    routes: [
        {
            path: '/',
            name: 'home',
            component: GameSelect
        },
        {
            path: '/words/categories',
            name: 'words-categories',
            component: WordsCategories
        },
        {
            path: '/words/category/:categoryId',
            name: 'words-category',
            component: WordsCategory
        },
        {
            path: '*',
            component: GameSelect
        }
    ]
})

const store = new Vuex.Store({
    state: {
        wordCategories: []
    },
    mutations: {
        addWordCategory (state, wordCategory) {
            state.wordCategories.push(wordCategory)
        }
    },
    getters: {
        getWordCategoryWords: (state) => (categoryId) => {
            let words = []
            let category = state.wordCategories.find(obj => obj.id == categoryId)
            if (typeof category !== 'undefined') {
                words = category.words
            }
            return words
        }
    }
})

new Vue({
    router,
    store
}).$mount('#app')